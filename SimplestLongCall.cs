using System;
using System.Threading;
using Android.App;
using Android.Views;
using Android.OS;
using Android.Widget;
using Java.Lang;
using Thread = System.Threading.Thread;

//This test case is meant to illustrate a scenario where there should be NO java code running on the thread making
//allocations of objects and triggering a nursery collection.  Another thread performs a Dalvik GC in tandem with a burst of activity
namespace AndroidApplication3
{
    [Activity(Label = "SimplestLongCall", MainLauncher = true, Icon = "@drawable/icon")]
    public class SimplestLongCall : Activity
    {
        private readonly Semaphore _GCTriggerJava = new Semaphore(0, int.MaxValue);
        private bool _Reeked = false;
        private Button _Button;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            SetContentView(Resource.Layout.Main);
            _Button = FindViewById<Button>(Resource.Id.MyButton);
            _Button.Text = "Start torture test";
            _Button.Click += (sender, args) =>
            {
                if (!_Reeked)
                {
                    _Button.Text = "Torture test running";
                    new Thread(ReekHavok).Start();
                    new Thread(ReekHavok3).Start();
                    _Reeked = true;
                }
            };
        }

        private void ReekHavok()
        {
            for (;;)
                Loop(1000);
        }

        private void Loop(int d)
        {
            _GCTriggerJava.Release();
            GC.Collect(0);
            for (var i = 0; i < d; ++i)
            {
                var r = new object();
            }
        }

        void ReekHavok3()
        {
            for (var i = 0; ; ++i)
            {
                _GCTriggerJava.WaitOne();
                JavaSystem.Gc();
            }
        }
    }
}

