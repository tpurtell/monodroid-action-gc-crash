using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Views;
using Android.OS;
using Android.Widget;
using Java.Lang;
using Thread = System.Threading.Thread;

//After seeing the gc-altstack test case which was disabled in wrench, I was wondering if I could remove the java GC from the crashing path.
//It turns out I could, by just throwing exceptions and catching them.  This would seem to point towards weird behavior in the context restorer.
namespace AndroidApplication3
{
    [Activity(Label = "gc-altstack-equivalent", MainLauncher = true, Icon = "@drawable/icon")]
    public class GcAltStackEquivalent : Activity
    {
        private readonly Semaphore _ExceptionTrigger = new Semaphore(0, int.MaxValue);
        private bool _Reeked = false;
        private Button _Button;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            SetContentView(Resource.Layout.Main);
            _Button = FindViewById<Button>(Resource.Id.MyButton);
            _Button.Text = "Start torture test";
            _Button.Click += (sender, args) =>
            {
                if (!_Reeked)
                {
                    _Button.Text = "Torture test running";
                    new Thread(ReekHavok).Start();
                    new Thread(ReekHavok3).Start();
                    _Reeked = true;
                }
            };
        }

        private void ReekHavok()
        {
            for (;;)
                Loop(1000);
        }

        private void Loop(int d)
        {
            _ExceptionTrigger.Release();
            GC.Collect(0);
            for (int i = 0; i < d; ++i)
            {
                var r = new object();
            }
        }

        void ReekHavok3()
        {
            for (var i = 0; ; ++i)
            {
                _ExceptionTrigger.WaitOne();
                for (int j = 0; j < 100; ++j)
                    try
                {
                    throw new System.Exception();
                }
                catch (System.Exception e)
                {
                }
            }
        }
    }
}

