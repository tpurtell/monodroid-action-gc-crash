using System;
using System.Threading;
using Android.App;
using Android.Views;
using Android.OS;
using Android.Widget;
using Java.Lang;
using Thread = System.Threading.Thread;

//This test case crashes the fastest.  It allocates a .NET object and uses it in one thread while java-gcing from another thread.
//Notably, it removes any use of handlers, actions, the java main thread, and it doesnt even call the mono gc explicitly.  Though
//the object allocations repeated in a loop should definitely trigger the mono garbage collector to do some work.
namespace AndroidApplication3
{
    [Activity(Label = "LessRandomizedLongCall", MainLauncher = true, Icon = "@drawable/icon")]
    public class LessRandomizedLongCall : Activity
    {
        private readonly Semaphore _GCTriggerJava = new Semaphore(0, int.MaxValue);
        private bool _Reeked = false;
        private Button _Button;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            SetContentView(Resource.Layout.Main);
            _Button = FindViewById<Button>(Resource.Id.MyButton);
            _Button.Text = "Start torture test";
            _Button.Click += (sender, args) =>
            {
                if (!_Reeked)
                {
                    _Button.Text = "Torture test running";
                    new Thread(ReekHavok).Start();
                    new Thread(ReekHavok3).Start();
                    _Reeked = true;
                }
            };
        }

        private void ReekHavok()
        {
            for (;;)
                Loop(1000);
        }

        private void Loop(int d)
        {
            _GCTriggerJava.Release();
            for (var i = 0; i < d; ++i)
            {
                var r = new Random();
                r.Next();
            }
        }

        void ReekHavok3()
        {
            for (var i = 0; ; ++i)
            {
                _GCTriggerJava.WaitOne();
                JavaSystem.Gc();
            }
        }
    }
}

