﻿using System;
using System.Threading;
using Android.App;
using Android.Views;
using Android.Widget;
using Android.OS;
using Thread = System.Threading.Thread;

//Original test case modified to wait until a button is clicked for easy ability to attach gdb
//Our real application posts many actions, and due to bugs seen in the 4.7 time frame, I suspected
//that something may have been wrong with the action handling.  I tried keeping static references to
//my actions, which didn't help.  Ultimately, I disassembled the Xamarin frameworks and found that
//internally the ThreadRunnable actually does this, so I was just performing a no-op.  
//
//The other test cases were derived from here by removing and manipulating things to 
//find the minimum set that crashes
namespace AndroidApplication3
{
    [Activity(Label = "CrossThreadPostGC", MainLauncher = true, Icon = "@drawable/icon")]
    public class CrossThreadPostGC : Activity
    {
        public int _Difference = 0;
        private Handler _Handler;
        private Button _Button;
        public bool _Tickle = true;
        public bool _Reeked = false;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            _Handler = new Handler(MainLooper);
            SetContentView(Resource.Layout.Main);
            _Button = FindViewById<Button>(Resource.Id.MyButton);
            _Button.Text = "Start torture test";
            _Button.Click += (sender, args) =>
            {
                if (!_Reeked)
                {
                    _Button.Text = "Torture test running";
                    new Thread(ReekHavok).Start();
                    new Thread(ReekHavok2).Start();
                    _Reeked = true;
                }
                _Tickle = true;
            };

        }

        void ReekHavok()
        {
            var r = new Random();
            for (var i = 0; ; ++i)
            {
                var j = i;
                var sb = new System.Text.StringBuilder();
                Action a = () =>
                {
                    sb.Append("foo");
                    var diff = Interlocked.Decrement(ref _Difference);
                    //I tuned this randomization to make the test crash frequently on my test device
                    if (r.Next() % 30 == 0 || _Tickle)
                        _Button.Text = string.Format("{0} + {1}MB + {2}", j, Debug.NativeHeapAllocatedSize / 1024.0 / 1024.0, diff);
                    _Tickle = false;
                };
                //show the difference so its clear we shouldn't be blowing out the number of grefs with pending runnables
                Interlocked.Increment(ref _Difference);
                _Handler.Post(a);
            }
        }
        void ReekHavok2()
        {
            var r = new Random();
            for (var i = 0; ; ++i)
            {
                var j = i;
                //I tuned this randomization to make the test crash frequently on my test device
                if (r.Next() % 20 == 0)
                    GC.Collect();
                Thread.Yield();
            }
        }
    }
}

