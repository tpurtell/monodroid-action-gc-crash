#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

#include <linux/user.h>
#include <sys/ptrace.h>
#include <asm/ptrace.h>

int main(int argc, char **argv)
{
	int options = PTRACE_O_TRACECLONE;
	for(int i = 1; i < argc; ++i)
	{
		if(ptrace(PTRACE_ATTACH, atoi(argv[i]), 0, 0) < 0) {
		  perror("ptrace");
		  _exit(1);
		}
		if(ptrace(PTRACE_SETOPTIONS,atoi(argv[i]),NULL,(void*)options ) < 0) {
		} else {
			printf("clone tracing\n");
		}
	}
	while(1) {
		int status;
		struct user regs;
		pid_t pid = waitpid(-1, &status, __WALL);
		if(!WIFSTOPPED(status)) {
			printf("%d: gone now\n", pid);
			if(pid == -1)
				break;
			continue;
		}
		if(ptrace(PTRACE_GETREGS, pid, 0, &regs) < 0)
			perror("ptrace/GETREGS");

			
		siginfo_t  sig;
		sig.si_signo = 0;

		if(ptrace(PTRACE_GETSIGINFO, pid, 0, &sig) >= 0) {
			printf("%d: signal %d, sicode %d, errno %d, addr %p\n", pid, sig.si_signo, sig.si_code, sig.si_errno, sig.si_addr); 
			//don't continue sending a syscall trap
			if(sig.si_signo == SIGTRAP) {
				sig.si_signo = 0;
			}
		} else {
			printf("%d: non-signal trace point\n", pid); 
		}
		printf("%d: pc=%p\n", pid, regs.regs.uregs[15]);
		printf("%d: sp=%p\n", pid, regs.regs.uregs[14]);
		printf("%d: lr=%p\n", pid, regs.regs.uregs[13]);
		printf("\n");

		if(ptrace(PTRACE_CONT, pid, 0, (void*)sig.si_signo) < 0)
			perror("ptrace/CONT");
	}
	return 0;
}


