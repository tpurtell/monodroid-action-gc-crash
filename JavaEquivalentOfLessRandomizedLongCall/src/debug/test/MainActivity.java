package debug.test;

import java.util.Random;
import java.util.concurrent.Semaphore;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.WindowManager;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        new Thread(new Randomizer()).start();
        new Thread(new GCer()).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    static Semaphore mGcKicker = new Semaphore(0);
    public static class Randomizer implements Runnable{

        @Override
        public void run() {
            for(;;)
                Loop(1000);
        }
        
    }

    private static void Loop(int d)
    {
        mGcKicker.release();
        for (int i = 0; i < d; ++i)
        {
            Random r = new Random();
            r.nextInt();
        }
    }    
    public static class GCer implements Runnable {

        @Override
        public void run() {
            for(;;) {
                try {
                    mGcKicker.acquire();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                System.gc();
            }
            
        }
        
    }
}
